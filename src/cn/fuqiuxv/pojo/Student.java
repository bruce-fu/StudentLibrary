package cn.fuqiuxv.pojo;

/**
 * Created by qiuxv on 2017/9/13.
 * encoding:UTF-8
 */
public class Student {

    private String id;
    private String studentId;
    private String name;
    private String sex;
    private String classString;
    private String remark;

    public Student(String id, String studentId, String name, String sex, String classString, String remark) {
        this.id = id;
        this.studentId = studentId;
        this.name = name;
        this.sex = sex;
        this.classString = classString;
        this.remark = remark;
    }
    public Student(String studentId, String name, String sex, String classString, String remark) {

        this.studentId = studentId;
        this.name = name;
        this.sex = sex;
        this.classString = classString;
        this.remark = remark;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public String getClassString() {
        return classString;
    }

    public String getRemark() {
        return remark;
    }

    public String getId() {
        return id;
    }
}
