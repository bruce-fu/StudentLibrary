package cn.fuqiuxv.servlet;

import cn.fuqiuxv.DAO.SelectSQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

/**
 * Created by qiuxv on 2017/9/13.
 * encoding:UTF-8
 * 还没有用上
 */
public class IndexServ extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ResultSet resultSet = SelectSQL.select();
        if(resultSet == null){
            System.out.println("resultSet为空");
        }
        request.getSession().setAttribute("resultSet",resultSet);

    }
}
