package cn.fuqiuxv.servlet;

import cn.fuqiuxv.DAO.DelectSQL;
import cn.fuqiuxv.DAO.InsertSQL;
import cn.fuqiuxv.DAO.UpdateSQL;
import cn.fuqiuxv.pojo.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by qiuxv on 2017/9/14.
 * encoding:UTF-8
 */
@WebServlet(urlPatterns = {"/update"},
        initParams = {
                @WebInitParam(name="INDEX_VIEW",value = "index.jsp")
        }

)
public class UpdateServ extends HttpServlet {
    private String INDEX_VIEW;
    public void init(){
        INDEX_VIEW = getServletConfig().getInitParameter("INDEX_VIEW");
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String id = request.getParameter("id");
        String studentId = request.getParameter("1");
        String name = request.getParameter("2");
        String sex= request.getParameter("3");
        String classString= request.getParameter("4");
        String remark= request.getParameter("5");
        if(remark.equals("")){
            remark="无";
        }

        //根据输入创建Student对象
        Student student = new Student(id,studentId,name,sex,classString,remark);
        System.out.println(id+"/"+studentId+"/"+name+"/"+sex+"/"+classString+"/"+remark);
        UpdateSQL.update(student);
        request.getRequestDispatcher(INDEX_VIEW).forward(request,response);
    }
}
