package cn.fuqiuxv.servlet;

import cn.fuqiuxv.DAO.InsertSQL;
import cn.fuqiuxv.pojo.Student;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * Created by qiuxv on 2017/9/13.
 * encoding:UTF-8
 */
@WebServlet(urlPatterns = {"/insert"},
        initParams = {
            @WebInitParam(name="INDEX_VIEW",value = "index.jsp")
        }

)
public class InsertServ extends javax.servlet.http.HttpServlet {
    private String INDEX_VIEW;
    public void init(){
        INDEX_VIEW = getServletConfig().getInitParameter("INDEX_VIEW");
    }
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String studentId = request.getParameter("studentId");
        String name = request.getParameter("name");
        String sex= request.getParameter("sex");
        String classString= request.getParameter("class");
        String remark= request.getParameter("remark");
        if(remark.equals("")){
            remark="无";
        }
        //根据输入创建Student对象
        Student student = new Student(studentId,name,sex,classString,remark);
        //将student插入数据库

        InsertSQL.insert(student);
        request.getRequestDispatcher(INDEX_VIEW).forward(request,response);

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
