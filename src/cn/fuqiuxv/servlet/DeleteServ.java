package cn.fuqiuxv.servlet;

import cn.fuqiuxv.DAO.DelectSQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by qiuxv on 2017/9/14.
 * encoding:UTF-8
 */
@WebServlet(urlPatterns = {"/delete"},
        initParams = {
                @WebInitParam(name="INDEX_VIEW",value = "index.jsp")
        }

)
public class DeleteServ extends HttpServlet {
    private String INDEX_VIEW;
    public void init(){
        INDEX_VIEW = getServletConfig().getInitParameter("INDEX_VIEW");
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        DelectSQL.delete(id);
        request.getRequestDispatcher(INDEX_VIEW).forward(request,response);

    }
}
