package cn.fuqiuxv.DAO;

import cn.fuqiuxv.pojo.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by qiuxv on 2017/9/14.
 * encoding:UTF-8
 */
public class UpdateSQL {
    static public void update(Student student){
        ConnectionMysql connectionMysql = new ConnectionMysql("jdbc:mysql://localhost:3306/student?useSSL=false","root","1234");
        Statement statement = connectionMysql.getStatement();
        Connection connection = connectionMysql.getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "update student set studentId = ?,name = ?,sex = ?,class = ?, remark = ? where ID = ?";
        try {

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,student.getStudentId());
            preparedStatement.setString(2,student.getName());
            preparedStatement.setString(3,student.getSex());
            preparedStatement.setString(4,student.getClassString());
            preparedStatement.setString(5,student.getRemark());
            preparedStatement.setString(6,student.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.close();


    }
}
