package cn.fuqiuxv.DAO;

import cn.fuqiuxv.pojo.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by qiuxv on 2017/9/14.
 * encoding:UTF-8
 */
public class DelectSQL {
    static public void delete(int id){
        ConnectionMysql connectionMysql = new ConnectionMysql("jdbc:mysql://localhost:3306/student?useSSL=false","root","1234");
        Statement statement = connectionMysql.getStatement();
        Connection connection = connectionMysql.getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "delete from student where id = ?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.close();


    }
}
