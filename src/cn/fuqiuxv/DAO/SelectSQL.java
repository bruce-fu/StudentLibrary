package cn.fuqiuxv.DAO;

import cn.fuqiuxv.pojo.Student;

import java.sql.*;

/**
 * Created by qiuxv on 2017/9/13.
 * encoding:UTF-8
 */
public class SelectSQL {
    //查找所有学生的信息
    static public ResultSet select(){
        //String name = student.getName();
        ConnectionMysql connectionMysql = new ConnectionMysql("jdbc:mysql://localhost:3306/stu" +
                "dent?useSSL=false","root","1234");
        Statement statement = connectionMysql.getStatement();
        Connection connection = connectionMysql.getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "select * from student";
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            boolean hasResult = statement.execute(sql);
            if(hasResult){
                resultSet = statement.getResultSet();
                return resultSet;
            }else{
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectionMysql.close();

        return null;
    }
}
